import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import "./globals";

import crypto from "crypto";
import randomBytes from "randombytes";

export default class App extends React.Component<{}, {
  hash: ?string,
  random: Array<number>,
}> {
  state = {}
  componentWillMount() {
    this.setState({
      hash: crypto.createHash('sha1').update('abc').digest('hex'),
      random: randomBytes(16)
    })
  }
  render() {
    return (
      <View style={styles.container}>
        <Text>{this.state.hash}</Text>
        <Text style={styles.description}>{this.state.hash}</Text>
        <Text>{JSON.stringify(this.state.random)}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
		paddingHorizontal: 12,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  description: {
    color: '#eee',
		paddingHorizontal: 12,
		paddingVertical: 9,
		backgroundColor: '#444',
	},
});
