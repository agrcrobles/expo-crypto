# expo-crypto

This is just a test that makes `crypto` to work on JavascriptCore using  `crypto-browserify version ^2.1.2`, a pure javascript tiny implementation of `randombytes`, and `expo`.

## Working Sample

```js
export default class App extends React.Component<{}, {
  hash: ?string,
  Array<number>,
}> {
  state = {}
  componentWillMount() {
    this.setState({
      hash: crypto.createHash('sha1').update('abc').digest('hex'),
      random: randomBytes(16)
    })
  }
  render() {
    return (
      <View>
        <Text>{this.state.hash}</Text>
        <Text>{JSON.stringify(this.state.random)}</Text>
      </View>
    );
  }
}
```

## More info

Somehow as a solution following discussion in https://expo.canny.io/feature-requests/p/crypto-api

## See

* https://github.com/agrcrobles/babel-preset-react-native-web3

## License

MIT